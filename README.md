# Leash

A remote control for your site

---

It uses WebSocket to remotelly control your site.

## Features

- Integration with common state management libraries
- Standard input types
- Custom input types handler
